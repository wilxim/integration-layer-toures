<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
				xmlns:tns="http://toures-balon/contract/messages"
			    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
			    exclude-result-prefixes="#default tns">
	
	<xsl:output method="text"/>
	<xsl:template match="/">
		<!-- TODO: Auto-generated template -->
		<xsl:variable name="int1" select="tns:touresbalon-order/tns:domain-events-cancel-order/tns:hilton/tns:order"/>
		<xsl:value-of select="substring($int1,1,10 )"></xsl:value-of>
		<xsl:variable name="int2" select="tns:touresbalon-order/tns:domain-events-cancel-order/tns:hilton/tns:ticket-number"/>
		<xsl:value-of select="substring($int2,1,10 )"></xsl:value-of>
		<xsl:variable name="int3" select="tns:touresbalon-order/tns:domain-events-cancel-order/tns:hilton/tns:id-client"/>
		<xsl:value-of select="substring($int3,1,10 )"></xsl:value-of>
		  
		
	</xsl:template>
</xsl:stylesheet>