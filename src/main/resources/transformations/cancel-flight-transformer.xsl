<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:tns="http://aerolineas-latinoamericanas/contract/messages"
	exclude-result-prefixes="#default tns">
	
	<xsl:output method ="xml" omit-xml-declaration="yes" indent="yes" encoding="utf-8" />
	
	<xsl:template match="/">
	
		<cancelRequest>
			<aircraftIdentifier><xsl:number format="00001"/></aircraftIdentifier>
			<externalKey><xsl:value-of select="tns:aircratline-message/tns:flight-leg/tns:domain-events-info/tns:operational-info/tns:departure-airport"/></externalKey>
			<description><xsl:value-of select="tns:aircratline-message/tns:flight-leg/tns:domain-events-info/tns:cancel-flight/tns:description"/></description>
			<date><xsl:text>2017-08-28T07:45:00</xsl:text></date>
		</cancelRequest>

	</xsl:template>
</xsl:stylesheet>