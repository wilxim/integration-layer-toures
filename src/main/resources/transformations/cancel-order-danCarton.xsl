<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
				xmlns:tns="http://toures-balon/contract/messages"
			    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
			    exclude-result-prefixes="#default tns">
	
	<xsl:output method="xml"/>
	<xsl:template match="/">
		
		<cancelRequest>
			<touresOrderIdentifier><xsl:number format="00001"/></touresOrderIdentifier>
			<order><xsl:value-of select="tns:touresbalon-order/tns:domain-events-cancel-order/tns:dan-carton/tns:order"/></order>
			<ticket-number><xsl:value-of select="tns:touresbalon-order/tns:domain-events-cancel-order/tns:dan-carton/tns:ticket-number"/></ticket-number>
			<cancel>true</cancel>
		</cancelRequest>

	</xsl:template>
</xsl:stylesheet>